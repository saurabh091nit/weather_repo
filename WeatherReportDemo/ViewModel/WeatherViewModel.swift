//
//  WeatherViewModel.swift
//  WeatherReportDemo
//
//  Created by Saurabh kumar on 17/07/21.
//

import Foundation

struct WeatherViewModel {

    let currentWeather: CurrentWeather
 
    private(set) var coordString = ""
    private(set) var windSpeedString = ""
    private(set) var windDegString = ""
    private(set) var locationString = ""
    private(set) var humidityString = ""
   
    private(set) var minTempString = 0.0
    private(set) var maxTempString = 0.0
    private(set) var weatherString = ""
    private(set) var country = ""
 
    init(currentWeather: CurrentWeather) {
        self.currentWeather = currentWeather
        updateProperties()
    }

    private mutating func updateProperties() {

        humidityString = setHumidityString(currentWeather: currentWeather)
        windSpeedString = setWindSpeedString(currentWeather: currentWeather)
        minTempString = setMinTempString(currentWeather: currentWeather)
        maxTempString = setMaxTempString(currentWeather: currentWeather)
        weatherString = setWeatherString(currentWeather: currentWeather)
        country = setCountryString(currentWeather: currentWeather)
 
     }
}

extension WeatherViewModel {

    private func setHumidityString(currentWeather: CurrentWeather) -> String {
        print(currentWeather.main.humidity)
        
        return "\(currentWeather.main.humidity)"
    }

    private func setWindSpeedString(currentWeather: CurrentWeather) -> String {
 
        return "\(currentWeather.wind.speed)"
    }
    
    
    private func setMinTempString(currentWeather: CurrentWeather) -> Double {
 
        return  currentWeather.main.tempMin ?? 0.0
    }
    
    private func setMaxTempString(currentWeather: CurrentWeather) -> Double {
 
        return  currentWeather.main.tempMax ?? 0.0
    }
        
    private func setWeatherString(currentWeather: CurrentWeather) -> String {
 
        return "\(String(describing: currentWeather.weather[0].description))"
    }
    
    private func setCountryString(currentWeather: CurrentWeather) -> String {
        
        print("\(String(describing: currentWeather.sys.country)), \(String(describing: currentWeather.name))")
 
        return "\(String(describing: currentWeather.sys.country)), \(String(describing: currentWeather.name))"
    }
    
     
}
