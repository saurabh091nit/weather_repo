//
//  APIManager.swift
//  WeatherReportDemo
//
//  Created by Saurabh kumar on 18/07/21.
//

import Foundation

protocol ServicesDelegate {

    func getServicesResponse(res: NSDictionary)
 }

class Services: NSObject {
    
    var delegate: ServicesDelegate?
}

class APIManager {
  
    func getWeather(url: NSURL,completion : @escaping (_ weather: CurrentWeather?, _ error: Error?) -> ()){
        URLSession.shared.dataTask(with: url as URL) { (data, urlResponse, error) in
            if let data = data {
                
                let jsonDecoder = JSONDecoder()
                let empData = try! jsonDecoder.decode(CurrentWeather.self, from: data)
                completion(empData,error)
            }
        }.resume()
    }
}

