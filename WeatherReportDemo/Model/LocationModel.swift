//
//  LocationModel.swift
//  WeatherReportDemo
//
//  Created by Saurabh kumar on 18/07/21.
//

import Foundation

struct LocationModel {
    
    var lat: Double
    var long: Double
    var address: String
    
    init(lat:Double, long:Double, address:String) {
        
        self.lat = lat
        self.long = long
        self.address = address

    }
    
    
}
