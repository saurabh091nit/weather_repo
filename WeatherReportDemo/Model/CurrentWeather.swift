//
//  CurrentWeather.swift
//  WeatherReportDemo
//
//  Created by Saurabh kumar on 17/07/21.
//

import Foundation

struct CurrentWeather: Codable {
    
    let coord: Coord
    let weather: [WeatherDetails]
    let base: String
    let main: Main
    let visibility: Int
    let wind: Wind
    let clouds: Clouds
    let dt: Int
    let sys: Sys
    let id: Int
    let name: String

}
