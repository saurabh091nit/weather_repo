//
//  Sys.swift
//  WeatherReportDemo
//
//  Created by Saurabh kumar on 17/07/21.
//

import Foundation

struct Sys: Codable {
    
    let type: Int
    let id: Int
    let country: String
    let sunrise: Int
    let sunset: Int
}
