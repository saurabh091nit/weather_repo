//
//  WeatherDetails.swift
//  WeatherReportDemo
//
//  Created by Saurabh kumar on 17/07/21.
//

import Foundation

struct WeatherDetails: Codable {
    let id: Int
    let main: String
    let description: String
    let icon: String
}
