//
//  Coord.swift
//  WeatherReportDemo
//
//  Created by Saurabh kumar on 17/07/21.
//

import Foundation
struct Coord: Codable {
    let lon: Double
    let lat: Double
}
