//
//  Wind.swift
//  WeatherReportDemo
//
//  Created by Saurabh kumar on 17/07/21.
//

import Foundation

struct Wind: Codable {
    let speed: Double
    let deg: Double
 }
