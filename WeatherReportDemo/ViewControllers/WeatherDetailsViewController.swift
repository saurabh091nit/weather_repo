//
//  WeatherDetailsViewController.swift
//  WeatherReportDemo
//
//  Created by Saurabh kumar on 17/07/21.
//

import UIKit

class WeatherDetailsViewController: UIViewController {

    var apiManager = APIManager()
    private(set) var windViewModel: WeatherViewModel?
    
    @IBOutlet weak var country: UILabel!
    @IBOutlet weak var weather_img: UIImageView!
    @IBOutlet weak var wind_lbl: UILabel!
    @IBOutlet weak var humidity_lbl: UILabel!
    @IBOutlet weak var minTemp_lbl: UILabel!
    @IBOutlet weak var maxTemp_lbl: UILabel!
    @IBOutlet weak var weather_lbl: UILabel!

    
    var lat = Double()
    var long = Double()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        getWeather()
    }
    
    var searchResult: CurrentWeather? {
        didSet {
            guard let searchResult = searchResult else { return }
            windViewModel = WeatherViewModel.init(currentWeather: searchResult)
            DispatchQueue.main.async {
                self.updateLabels()
            }
        }
    }
}

extension WeatherDetailsViewController {

    private func updateLabels() {
        guard let windViewModel = windViewModel else { return }
        
        country.text =  windViewModel.country
        wind_lbl.text =  windViewModel.windSpeedString
        humidity_lbl.text =  windViewModel.humidityString
        weather_lbl.text = windViewModel.weatherString
        minTemp_lbl.text = String(format: "%.0f",  windViewModel.minTempString - 273.15)
        maxTemp_lbl.text = String(format: "%.0f",  windViewModel.maxTempString - 273.15)
     }

    private func getWeather() {
        
        
        
        let sourcesURL = URL(string: "https://api.openweathermap.org/data/2.5/weather?lat=\(Double(lat))&lon=\(Double(long))&appid=6b015ea9bb79d2d88082f6e4ab996254")!
        apiManager.getWeather(url:sourcesURL as NSURL) { (weather, error) in
            if let error = error {
                print("Get weather error: \(error.localizedDescription)")
                return
            }
            guard let weather = weather  else { return }
            self.searchResult = weather
            print("Current Weather Object:")
            print(weather)
        }
    }
}
