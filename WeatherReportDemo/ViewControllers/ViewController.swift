//
//  ViewController.swift
//  WeatherReportDemo
//
//  Created by Saurabh kumar on 17/07/21.
//

import UIKit
import CoreLocation
import MapKit

class ViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var addressListTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    
    let newPin = MKPointAnnotation()
    var locationArr = [LocationModel]()
    var locationManager: CLLocationManager!
    var geocoder = CLGeocoder()
    var lat = Double()
    var long = Double()
    var addressArr = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        searchBar.delegate = self
        
        getlocation()
        
    }
    
    func getlocation()  {
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        
    }
    
    func get_lat_long(address: String) {
        
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(address, completionHandler: { [self](placemarks, error) -> Void in
            if((error) != nil){
                print("Error", error ?? "")
            }
            if let placemark = placemarks?.first {
                let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
                print("Lat: \(coordinates.latitude) -- Long: \(coordinates.longitude)")
                
                let span = MKCoordinateSpan(latitudeDelta: (placemark.location?.coordinate.latitude)!, longitudeDelta: (placemark.location?.coordinate.latitude)!)
                let region = MKCoordinateRegion(center: coordinates, span: span)
                mapView.setRegion(region, animated: true)
                
                newPin.coordinate = placemark.location!.coordinate
                mapView.addAnnotation(newPin)
                
                lat = (placemark.location!.coordinate.latitude)
                long = (placemark.location!.coordinate.longitude)
                
                getAddress(loc: placemark.location!)
                
            }
        })
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.locationArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:AddressListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell")! as! AddressListTableViewCell
        cell.address_lbl.text = self.locationArr[indexPath.row].address
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WeatherDetailsViewController") as? WeatherDetailsViewController
        
        vc?.lat = self.locationArr[indexPath.row].lat
        vc?.long = self.locationArr[indexPath.row].long
        
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.locationArr.remove(at: indexPath.row)
            self.addressListTableView.beginUpdates()
            self.addressListTableView.deleteRows(at: [indexPath], with: .automatic)
            self.addressListTableView.endUpdates()
        }
    }
}

extension ViewController: CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            mapView.removeAnnotation(newPin)
            
            print("Lat : \(location.coordinate.latitude) \nLng : \(location.coordinate.longitude)")
            
            let span = MKCoordinateSpan(latitudeDelta: location.coordinate.latitude, longitudeDelta: location.coordinate.longitude)
            let region = MKCoordinateRegion(center: location.coordinate, span: span)
            mapView.setRegion(region, animated: true)
            
            newPin.coordinate = location.coordinate
            mapView.addAnnotation(newPin)
            
            lat = (location.coordinate.latitude)
            long = (location.coordinate.longitude)
            getAddress(loc: location)
            
        }
    }
    
    func getAddress(loc: CLLocation) {
        
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(loc) { (placemarksArray, error) in
            
            if (placemarksArray?.count)! > 0 {
                
                let placemark = placemarksArray?.first
                let number = placemark!.subThoroughfare ?? " "
                let bairro = placemark!.subLocality ?? " "
                let street = placemark!.thoroughfare ?? " "
                
                self.addressArr.add("\(street), \(number) - \(bairro)")
                self.searchBar.text = "\(street), \(number) - \(bairro)"
                
                self.locationArr.append(LocationModel(lat: loc.coordinate.latitude, long: loc.coordinate.longitude, address: self.searchBar.text!))
                
                DispatchQueue.main.async {
                    
                    self.addressListTableView.reloadData()
                }
            }
        }
    }
    
   

}

extension ViewController: UISearchBarDelegate{
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
    }
    
    private func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        get_lat_long(address: searchBar.text ?? "")
        self.searchBar.endEditing(true)
    }
    
    private func searchBarShouldEndEditing(searchBar: UISearchBar) -> Bool {
        
        return true
    }
}



